var socket = io.connect('http://localhost:8080'/*,{'forceNew':true}*/);

socket.on('messages', function(data){
	console.log(data);
	render(data);
});

function render(data){
	var html = data.map(function(data, index){
		return (`<div>
					<strong>${data.usuario}</strong>:
					<em>${data.message}</em>
				</div>`);
	}).join(" ");

	document.getElementById("recibir").innerHTML = html;
}

function addMessage(event){
	var payload = {
		idSocket: socket.id,
		usuario: document.getElementById("username").value,
		message: document.getElementById("texto").value
	};

	console.log(payload);

	socket.emit('new-message', payload);
	return false;
}