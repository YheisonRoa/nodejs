
var express = require('express');
var server = require('http').Server(app);
var io = require('socket.io')(server);
var app = express();

var messages = [];

//app.use(express.static('public'));

io.on('connection', function(socket){
	var idSocket = socket.id;
	console.log(idSocket);
	console.log("alguien se ha conectado con Sockets");
	//socket.emit('messages',messages);

	socket.on('new-message',function(data){
		console.log(data.id);
		console.log("Nuevo mensaje de un cliente");
		messages.push(data);
		
		//var cliente_especifico = io.sockets.socket(id);
		//cliente_especifico.emit();
		
		io.sockets.emit('messages', messages);
	});
})

server.listen(8080,function(){
	console.log("Servidor corriendo");
});

/*
var socket = require('socket.io');
var express = require('express');
var http = require('http');

var app = express();
var server = http.createServer(app);

var io = socket.listen(server);


var messages = [
	{
		id: 1,
		text: "Hola, que tal?",
		author: "Yheison"
	}
];

io.socket.on('connection', function(data){
	console.log("Nuevo Cliente");

	client.on('message', function(){			
		io.socket.emit('message', {
			usuario: messages.author,
			message: message.text,
		})
	})
});

server.listen(8080,function(){
	console.log("Servidor corriendo");
});
*/